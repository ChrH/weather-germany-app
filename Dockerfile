FROM python:3.10-slim-bullseye

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && \
    apt-get -y upgrade && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY app/requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY app/app.py .

EXPOSE 7777

ENTRYPOINT ["python3", "app.py"]
