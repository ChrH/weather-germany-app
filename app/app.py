#!/usr/bin/env python3

from wetterdienst import Settings
from wetterdienst.provider.dwd.observation import DwdObservationRequest, DwdObservationDataset, DwdObservationPeriod, DwdObservationResolution
from dash import Dash, html, dcc, Input, Output
import dash_bootstrap_components as dbc
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import datetime

# wetterdienst settings
print(Settings)
Settings.si_units=False
#print(DwdObservationRequest.discover().keys())
#print(list(DwdObservationDataset))
#print(list(DwdObservationPeriod))
#print(list(DwdObservationResolution))
PARAM = DwdObservationDataset.TEMPERATURE_AIR
RESOLUTION = DwdObservationResolution.HOURLY #MINUTE_10
PERIOD = DwdObservationPeriod.RECENT
TEMPERATURE = "temperature_air_mean_200" #"temperature_air_mean_005"
HUMIDITY = "humidity"
END_DATE = datetime.datetime.now()
START_DATE = END_DATE - datetime.timedelta(days=7)

states = [
    {"label": "All", "value": "All"},
    {"label": "Baden-Württemberg", "value": "Baden-Württemberg"},
    {"label": "Bayern", "value": "Bayern"},
    {"label": "Berlin", "value": "Berlin"},
    {"label": "Brandenburg", "value": "Brandenburg"},
    {"label": "Bremen", "value": "Bremen"},
    {"label": "Hamburg", "value": "Hamburg"},
    {"label": "Hessen", "value": "Hessen"},
    {"label": "Mecklenburg-Vorpommern", "value": "Mecklenburg-Vorpommern"},
    {"label": "Niedersachsen", "value": "Niedersachsen"},
    {"label": "Nordrhein-Westfalen", "value": "Nordrhein-Westfalen"},
    {"label": "Rheinland-Pfalz", "value": "Rheinland-Pfalz"},
    {"label": "Saarland", "value": "Saarland"},
    {"label": "Sachsen", "value": "Sachsen"},
    {"label": "Sachsen-Anhalt", "value": "Sachsen-Anhalt"},
    {"label": "Schleswig-Holstein", "value": "Schleswig-Holstein"},
    {"label": "Thüringen", "value": "Thüringen"},
    ]

app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

left = html.Div([
    html.Div([
        dbc.Label("State:"),
        dcc.Dropdown(id="select_state",
            options=states,
            value="All",
            ),
        ],
        ),
    html.Br(),
    html.Div([
        dbc.Label("Weather station location:"),
        dcc.Dropdown(id="select_station",
             ),
        ],
        ),
    html.Br(),
    html.Div([
        dbc.Label("Use data within radius:"),
        dcc.Slider(0, 100,
                step=None,
                marks={i: str(i) + " km" for i in range(0, 110, 10)},
                value=10,
                id="radius_slider",
                ),
        ],
        ),
    html.Br(),
    dcc.Graph(id='figure_temp_hum', figure={})
    ])

app.layout = dbc.Container([
    html.H2("7-day temperature and humidity data for Germany", style={'text-align': 'center'}),
    dbc.Row([
        dbc.Col(left, md=8),
        dbc.Col(dcc.Graph(id='figure_map', figure={}), md=4)
    ], align="center"),
    html.Footer([
        html.A("Source", target="_blank", href="https://gitlab.com/ChrH/weather-germany-app"),
        ". Data provided by the ",
        html.A("DWD (Deutscher Wetterdienst)", target="_blank", href="https://www.dwd.de/EN/ourservices/opendata/opendata.html"),
        " via the ",
        html.A("wetterdienst library", target="_blank", href="https://github.com/earthobservations/wetterdienst"),
        "."
        ])
    ])

# query all stations
request = DwdObservationRequest(
                            parameter=PARAM,
                            resolution=RESOLUTION,
                            period=PERIOD,
                            start_date=START_DATE.date().isoformat(),
                            end_date=END_DATE.date().isoformat(),
                          )
df_all = request.all().df

@app.callback(
        [Output(component_id='select_station', component_property='options'),
         Output(component_id='select_station', component_property='value')],
        Input(component_id='select_state', component_property='value')
        )
def get_station_names(selected_state):
    print(f"State selected: {selected_state}")
    if selected_state == "All":
        station_names = sorted(df_all['name'].values.tolist())
    else:
        station_names = sorted(df_all[df_all['state'] == selected_state]['name'].values.tolist())
    opt = [{"label":i, "value": i} for i in station_names]
    return opt, station_names[0]

@app.callback(
        [Output(component_id='figure_temp_hum', component_property='figure'),
         Output(component_id='figure_map', component_property='figure')],
        [Input(component_id='select_station', component_property='value'),
         Input(component_id='radius_slider', component_property='value')]
        )
def update_figures(selected_station, radius):
    LATITUDE, LONGITUDE = request.filter_by_name(name=selected_station).df.iloc[0][['latitude', 'longitude']].tolist()
    print(f"Selected station {selected_station} at coordinates {LATITUDE}, {LONGITUDE}. Radius: {radius} km")
    # Get data from all stations within radius around selected stations
    df = request.filter_by_distance(latitude=LATITUDE, longitude=LONGITUDE, distance=radius, unit="km").values.all().df
    # group station data
    df = df.groupby(['date', 'parameter'], as_index=False).mean().dropna()
    mean_temp = df[df['parameter'] == TEMPERATURE]
    mean_hum = df[df['parameter'] == HUMIDITY]
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(
            go.Scatter(x=mean_temp["date"], y=mean_temp["value"], name="Temperature", mode="lines+markers", marker={"symbol":"circle"}),
                secondary_y=False,
                )
    fig.add_trace(
            go.Scatter(x=mean_hum["date"], y=mean_hum["value"], name="Humidity", mode="lines+markers", marker={"symbol":"diamond"}),
                secondary_y=True,
                )
    fig.update_layout(title=f"Averages within {radius} km of {selected_station}")
    fig.update_xaxes(title_text="Date")
    fig.update_yaxes(title_text="Temperature [°C]", secondary_y=False)
    fig.update_yaxes(title_text="Humidity [%]", showgrid=False, secondary_y=True)

    fig_map = go.Figure(data=go.Scattergeo(
        lat=[LATITUDE],
        lon=[LONGITUDE],
        text=[selected_station],
        opacity=0.9,
        marker={"symbol":"cross", "color":"black", "size":12},
        ))
    fig_map.update_geos(
            visible=True,
            resolution=50,
            showcountries=True,
            countrycolor="blue",
            projection_type="mercator",
            lataxis_range=[46, 57], lonaxis_range=[2, 18]
            )
    fig_map.update_layout(width=500, height=800)

    return fig, fig_map

if __name__ == '__main__':
    app.run_server(host="0.0.0.0", debug=True, port=7777)
