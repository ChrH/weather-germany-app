# 7-Day temperature and humidity data for Germany
This is a simple dashboard app written using Plotly/Dash to explore weather data. It enables averaging over data within a radius from 0 to 100 km around the chosen weather station.
All weather data is provided by the [DWD (Deutscher Wetterdienst)](https://www.dwd.de/EN/ourservices/opendata/opendata.html) via the [wetterdienst library](https://github.com/earthobservations/wetterdienst).

# Run
The app can be run directly:
`./app.py`

Then open [http://0.0.0.0:7777](http://0.0.0.0:7777) in your browser.

## Docker
Alternatively, there is a (rather large) Docker image on Docker Hub.

Download:

`docker pull christophhoelzl/weather-germany-app`

Run:

`docker run -p 7777:7777 christophhoelzl/weather-germany-app`

## Windows

Under Windows, the running Docker image is accessible through [http://docker.for.win.localhost:7777](http://docker.for.win.localhost:7777) in your browser.
